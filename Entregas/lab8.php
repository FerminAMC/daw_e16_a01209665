<!Doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Lab 8</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
    <div class="container"><div class="row"><?php
    	echo "<table class='table table-striped'>";
        echo "<tr><td><h1>Arreglo de números aleatorios, su promedio y mediana:</td></tr></h1>";
    	$numArray = array();
    	$aux = 0;
    	for($i = 0; $i < rand(0, 50); $i++){
    		array_push($numArray, rand(0, 100));
    	}
    	sort($numArray);
    	echo "<tr><td><p>";
    	for($j = 0; $j < count($numArray); $j++){
    		$aux += $numArray[$j];
    		echo $numArray[$j] . ", ";
    	}
    	echo "</p></td></tr>";
    	echo "</br><tr><td><p>El promedio del arreglo es: " . $aux/count($numArray) . "</p></td><tr></br>";
    	
    	if(count($numArray) % 2 == 0){
    	    echo "<tr><td><p>La mediana del arreglo es: " . ($numArray[count($numArray)/2 - 1] + $numArray[(count($numArray)/2)])/2 . "</p></td></tr></br>";    
    	} else{
    	    echo "<tr><td><p>La mediana del arrego es: " . ($numArray[count($numArray)/2]) . "</p></td></tr></br>";
    	}
    	echo "</table>";
    	echo "<table class='table table-bordered'>";
    	echo "<tr><td>Sin potencia</td><td>Al cuadrado</td><td>Al cubo</td></tr>";
    	for($k = 1; $k < 10; $k++):
    	    echo "<tr class='text-center'><td>$k</td><td>" . pow($k, 2) . "</td>" . "<td>" . pow($k, 3) . "</td>" . "</tr>";
    	endfor; 
    	echo "</table>";
    	
    	$jsonurl = "http://api.openweathermap.org/data/2.5/weather?q=London,uk";
        $json = file_get_contents($jsonurl);
        $data=json_decode($json,true);
        echo $data['main']['temp']."<br>";
    	echo "<table class='table table-bordered'><tr><td>La temperatura actual en Londres: ". $data['main']['temp']. "</td></tr></table>";
    	echo "<table class='table table-bordered'><tr><td><p>¿Qué hace la función phpinfo()? La
    	 función phpinfo() muestra información sobre la configuración PHP. Devuelve información como los datos de
    	 la lisencia PHP, los valores locales y master de PHP y los valores de todas las variables predefinidas
    	  EGPCS (Environment, GET, POST, Cookie, Server).</p></td></tr>
    	  <tr><td><p>¿Qué cambios tendrías que hacer en la configuración del 
    	  servidor para que pudiera ser apto en un ambiente de producción? Si el servidor soporta PHP no 
    	  es necesario hacer nada, basta con crear los archivos .php. Si el servidor no soporta PHP
    	   es necesario instarlar un servidor web, instalar PHP e instalar una base de datos como MySQL. 
    	   Esta página tiene todas las instrucciones: http://php.net/manual/en/install.php</p></td></tr>
    	  <tr><td><p>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del
    	  cliente, se ejecuta del lado del servidor? Lo que hace el servidor es leer el código PHP e interpretarlo para poder 
    	  regresar un archivo en html al usuario</p></td></tr></table>"
    ?></div></div>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</body>
</html>