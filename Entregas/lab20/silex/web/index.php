<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app->get('/hello/{name}', function ($name) {
    $i;
    return 'Hello ' . $name . '!';
});

$app->get('/fun2', function(){
    return "¿A qué se refiere la descentralización de servicios web? Son servicios que están en varios servidores en lugares en caso de que un servidor no esté disponible </br> 
    ¿Cómo puede implementarse un entorno con servicios web disponibles aún cuando falle un servidor? En caso de que la respuesta de un servidor termine en error se hace una llamada a otro servidor para el mismo servicio";
});

$app->run();
?>