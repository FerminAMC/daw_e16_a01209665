<div class="container">
  <div class="row">
    <div class="col-xs-8 col-xs-offset-2">
      <form action="login.php" method="post">
        <div class="form-group">
          <label for="inputName">Name <p class="text-danger 
          <?php 
          if($_POST["name"] != ""){echo "hidden";} else echo "show";
          ?>">*campo obligatorio</p></label>
          <input type="text" id="inputName" name="name" class="form-control" placeholder="John Appleseed"/>
        </div>
        <div class="form-group">
          <label for="inputEmail">Email <p class="text-danger 
          <?php 
          if($_POST["email"] != ""){echo "hidden";} else echo "show";
          ?>">*campo obligatorio</p></label>
          <input type="email" class="form-control" id="inputEmail" placeholder="example@email.com" name="email"/>
        </div>
        <div class="form-group">
          <label for="inputPass">Password <p class="text-danger 
          <?php 
          if($_POST["password"] != ""){echo "hidden";} else echo "show";
          ?>">*campo obligatorio</p></label>
          <input type="password" class="form-control" id="inputPass" placeholder="password" name="password"/>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
    </div>
  </div>
</div>