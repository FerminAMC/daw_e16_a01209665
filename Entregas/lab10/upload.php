<?php
include_once "header.php";
include_once "navbar.php";

    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "
            <div class='container'>
                <div class='row'>
                    <div class'col-xs-12 bg-danger'>
                        <div class='well'>
                            <p class='text-center'>Archivo subido correctamente</p>
                        </div>
                    </div>
                </div>
            </div>
            ";
        } else {
            echo "
            <div class='container'>
                <div class='row'>
                    <div class'col-xs-12 bg-danger'>
                        <div class='well'>
                            <p class='text-center'>Archivo no subido</p>
                        </div>
                    </div>
                </div>
            </div>
            ";
        }
    }   

include_once "footer.html";
?>