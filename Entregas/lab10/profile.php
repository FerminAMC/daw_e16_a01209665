<div class="container-fluid">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                    <img src="http://seekershub.org/ans-blog/wp-content/uploads/2015/12/help-dog-picture.jpg" class="img-thumbnail img-responsive">
                </div>
            </div>
            <table class="table table-bordered">
                <tr class="text-center">
                    <td><?php 
                        echo $_SESSION["name"];
                    ?></td>
                </tr>
                <tr class="text-center">
                    <td><?php 
                        echo $_SESSION["email"];
                    ?></td>
                </tr>
                <tr>
                    <td>
                        <button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="collapse" href="#contraseña" aria-expanded="false" aria-controls="contraseña">Cambiar contraseña</button>
                        <div class="collapse" id="contraseña">
                            <div class="well">
                                <form action="changePass.php" method="post">
                                    <label for="oldPass">Escribe la contraseña anterior</label>
                                    <input type="password" class="form-control" id="oldPass" placeholder="old password" name="oldPassword"/>
                                    <label for="oldPass">Escribe la contraseña nueva</label>
                                    <input type="password" class="form-control" id="newPass" placeholder="new password" name="newPassword"/>
                                    <input type="submit" value="Cambiar" class="btn btn-danger"/>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="collapse" href="#foto" aria-expanded="false" aria-controls="foto">Subir foto al servidor</button>
                        <div class="collapse" id="foto">
                            <div class="well">
                                <form action="upload.php" method="post" enctype="multipart/form-data">
                                    <input class="form-control" type="file" name="fileToUpload" id="fileToUpload">
                                    <input class="form-control" type="submit" value="Upload Image" name="submit">
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-xs-8 col-xs-offset-2">
            <div class="well">
                <table class="table table-bordered">
                    <tr>
                        <td><bold>¿Por qué es importante hacer un session_unset() y luego un session_destroy()?</bold> Para eliminar los datos almacenados en $_SESSION e indicar que la sesión ha terminado</td>
                    </tr>
                    <tr>
                        <td><bold>¿Cuál es la diferencia entre una variable de sesión y una cookie?</bold> Las sesiones son archivos almacenados del lado del servidor que contienen información del usuario mientras que las cookies son archivos del lado del cliente</td>
                    </tr>
                    <tr>
                        <td><bold>¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de archivos cuando sube una foto con el mismo nombre?</bold> No tengo idea de que técnicas utilicen en Facebook pero probablemente utilizan cosas como identificadores únicos como el tiempo. Así pueden evitar problemas con archivos con los mismos nombres</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>