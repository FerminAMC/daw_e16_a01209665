<?php 
include_once "header.php";
include_once "navbar.php";
if($_POST["oldPassword"] == $_SESSION["password"]){
    $_SESSION["password"] = $_POST["newPassword"];
    echo "
    <div class='container'>
        <div class='row'>
            <div class'col-xs-12 bg-success'>
                <div class='well'>
                    <p class='text-center'>Contraseña cambiada</p>
                </div>
            </div>
        </div>
    </div>
    ";
    
}
else{
    echo "
    <div class='container'>
        <div class='row'>
            <div class'col-xs-12 bg-danger'>
                <div class='well'>
                    <p class='text-center'>Contraseña no cambiada</p>
                </div>
            </div>
        </div>
    </div>
    ";
}
include_once "footer.html";
?>