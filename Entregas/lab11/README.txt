¿Qué es ODBC? Es un api para accesar a sistemas de manejo de bases de datos.

¿Qué es SQL Injection? Es una técnica de programación utilizada para atacar aplicaciones con bases de datos a través de SQL malicioso. 

¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection? No utilizar SQL y utilizar una base de datos orientada a objetos tipo JSON. Si eso no es una opción, se deben utilizar parametros con SQL dinámico y validar todos los inputs de la aplicación.

