
var ref = new Firebase("https://daw-bd-labs.firebaseio.com");
var USERS_LOCATION = 'https://daw-bd-labs.firebaseio.com/user';

function go() {
  var userId = prompt('Username?', 'Guest');
  checkIfUserExists(userId);
}



function userExistsCallback(userId, exists) {
  if (exists) {
    alert('user ' + userId + ' exists!');
  } else {
    alert('user ' + userId + ' does not exist!');
  }
}

// Tests to see if /users/<userId> has any data. 
function checkIfUserExists(userId) {
  var usersRef = new Firebase(USERS_LOCATION);
  usersRef.child(userId).once('value', function(snapshot) {
    var exists = (snapshot.val() !== null);
	if(exists){
		var prueba = (snapshot.val().nickname);
		document.getElementById("dataName").innerHTML = prueba;
	}
    userExistsCallback(userId, exists);
  });
}

function setInfo(){
	var userRef = new Firebase(USERS_LOCATION);
	$nameData = document.getElementById("name").value;
	$heightData = document.getElementById("height").value;
	$weightData = document.getElementById("weight").value;
	userRef.child($nameData + "/height").set($heightData);
	userRef.child($nameData + "/weight").set($weightData);
	userRef.child($nameData).once('value', function(snapshot) {
		document.getElementById("dataName").innerHTML = $nameData;
		document.getElementById("dataHeight").innerHTML = snapshot.val().height;
		document.getElementById("dataWeight").innerHTML = snapshot.val().weight;
	});
}

function searchInfo(){
	var userRef = new Firebase(USERS_LOCATION);
	$nameData = document.getElementById("searchName").value;
	userRef.child($nameData).once('value', function(snapshot){
		var exists = (snapshot.val() !== null);
		if(exists){
			document.getElementById("displayInfo").innerHTML = "Altura: " + snapshot.val().height + " m, Peso: " + snapshot.val().weight + " kg";
		}
		else{
			document.getElementById("displayInfo").innerHTML = "El usuario no existe";
		}
	});
}

function updateInfo(){
	var userRef = new Firebase(USERS_LOCATION);
	$nameData = document.getElementById("updateName").value;
	$newWeight = document.getElementById("newWeight").value;
	$newHeight = document.getElementById("newHeight").value;
	userRef.child($nameData).once('value', function(snapshot){
		var exists = (snapshot.val() !== null);
		if(exists){
			userRef.child($nameData).update({"weight":$newWeight});
			userRef.child($nameData).update({"height":$newHeight});
			alert("usuariio modificado");
		}
		else{
			alert("El usuario no existe");
		}
	});
}

function deleteInfo(){
	var userRef = new Firebase(USERS_LOCATION);
	$nameData = document.getElementById("nameDelete").value;
	userRef.child($nameData).once('value', function(snapshot){
		var exists = (snapshot.val() !== null);
		if(exists){
			userRef.child($nameData).remove();
			alert("usuario eliminado");
		}
		else{
			alert("El usuario no existe");
		}
	});
}








