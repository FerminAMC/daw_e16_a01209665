<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 bg-primary">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <a class="btn btn-primary" href="#">Lab 9</a>
                            <button data-toggle="dropdown" class="btn btn-primary pull-right dropdown-toggle 
                            <?php if($_SESSION["name"] == ""){echo "hidden";}
                            else echo "show";
                            ?>
                            "><?php echo $_SESSION["name"];?><span class="caret"></span></button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#">
                                    <?php echo $_SESSION["email"];?>
                                </a></li>
                                <li class="divider"></li>
                                <li><a href="logout.php">Log out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>