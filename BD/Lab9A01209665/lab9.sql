
Set Dateformat dmy
Select sum(Cantidad) as 'Cantidades totales', sum(Cantidad * ((Costo * PorcentajeImpuesto) + Costo)) as 'Importe total'
From Entregan e, Materiales m
Where e.Clave = m.Clave and fecha >= '01/01/1997' and fecha <= '31/12/1997'

Select RazonSocial,count(Numero) as 'Num entregas', sum(Cantidad * ((Costo * PorcentajeImpuesto) + Costo)) as 'Importe total'
From Entregan e, Proveedores p, Materiales m
Where p.RFC = e.RFC and m.Clave = e.Clave
Group by RazonSocial

Select m.Clave, Descripcion, sum(Cantidad) as 'Cantidad total', 
min(Cantidad) as 'Cantidad minima', max(Cantidad) as 'Cantidad maxima', 
sum(Cantidad * ((Costo * PorcentajeImpuesto) + Costo)) as 'Importe total'
From Materiales m, Entregan e
Where m.Clave = e.Clave
Group by  m.Clave, Descripcion

Select RazonSocial, m.Clave, Descripcion, avg(Cantidad) as 'Cantidad promedio'
From Entregan e, Proveedores p, Materiales m  
Where m.Clave = e.Clave and e.RFC = p.RFC 
Group by RazonSocial, m.Clave, Descripcion 
Having avg(Cantidad) >500
Order by 'Cantidad promedio' asc

Select RazonSocial, m.Clave, Descripcion, avg(Cantidad) as 'Cantidad promedio'
From Entregan e, Proveedores p, Materiales m  
Where m.Clave = e.Clave and e.RFC = p.RFC 
Group by RazonSocial, m.Clave, Descripcion 
Having avg(Cantidad) >450 or avg(Cantidad) < 370
Order by 'Cantidad promedio' asc

INSERT INTO Materiales VALUES (1440, 'Pintura Prueba', 300.00, 2.88)
INSERT INTO Materiales VALUES (1450, 'Pintura Prueba', 300.00, 2.90)
INSERT INTO Materiales VALUES (1460, 'Pintura Prueba', 300.00, 2.92)
INSERT INTO Materiales VALUES (1470, 'Pintura Prueba', 300.00, 2.94)
INSERT INTO Materiales VALUES (1480, 'Pintura Prueba', 300.00, 2.96)

Select Clave, Descripcion
From Materiales
Where Clave NOT IN(
	Select Clave 
	From Entregan)

Select Distinct RazonSocial
From Proveedores p, Entregan e
Where p.RFC = e.RFC and Numero IN(
	Select Numero
	From Proyectos
	Where Denominacion = 'Vamos Mexico' or Denominacion = 'Queretaro Limpio')

Select Descripcion
From Materiales m, Entregan e
Where m.Clave = e.Clave and Numero NOT IN(
	Select Numero 
	From Proyectos
	Where Denominacion = 'CIT Yucatán')

Select RazonSocial, avg(Cantidad) as 'Cantidad promedio'
From Proveedores p, Entregan e
Where p.RFC = e.RFC 
Group by RazonSocial
Having avg(Cantidad) > (
	Select avg(Cantidad)
	From Entregan
	Where RFC = 'VAGO780901')

Select e.RFC, RazonSocial
From Proveedores p, Entregan e
Where p.RFC = e.RFC and fecha >= '01/01/2000' and fecha <= '31/12/2000' and Numero IN(
	Select Numero
	From Proyectos
	Where Denominacion = 'Infonavit Durango')
Group by e.RFC, RazonSocial
Having sum(Cantidad) > (
	Select sum(Cantidad)
	From Entregan
	Where fecha >= '01/01/2001' and fecha <= '31/12/2001')